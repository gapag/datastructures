from setuptools import setup, find_namespace_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="gapagds",
    version="0.0.2",
    author="gapag",
    namespace_packages=['gapag'],
    author_email="gp@gapag.st",
    description="various data structures",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://bitbucket.org/gapag/datastructures",
    package_dir={'': 'src'},
    packages=find_namespace_packages(where='src'),
    
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # # pytest specifics
    # setup_requires=["pytest-runner", ],
    # tests_require=["pytest"],
)