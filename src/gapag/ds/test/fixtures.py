from collections import namedtuple
from copy import deepcopy

TI = namedtuple('Fixture', ['entries', 
                            'under_test_cls', 
                            'original_cls', 
                            'retrieve'])

class MappingFixture:
    
    original_cls = None
    under_test_cls = None
    retrieve_default = lambda m, x: m[x]
    
    def __init__(self, entries, under_test_cls=None, original_cls = None,
                 retrieve = None):
        self.original = original_cls() \
                        if original_cls \
                        else type(self).original_cls() 
        self.under_test = under_test_cls() \
                        if under_test_cls \
                        else type(self).under_test_cls()
        self.retrieve = type(self).retrieve_default or retrieve
        for item in (self.original, self.under_test):
            for x in entries:
                item[x[0]] = deepcopy(x[1])
            self.check_invariants(item)
        print("E mo")
    
    def protect(self):
        raise NotImplementedError()
    
    def modifies(self, test, *locations):
        for k in self.original.keys():
            if not test(k, *locations):
                try:
                    uti = self.retrieve(self.under_test, k)
                except KeyError:
                    assert False, f"{k} not in under_test"
                else:
                    try:
                        oi = self.retrieve(self.original, k)
                    except:
                        assert False, f"{k} not in under_test"
                    else:
                        assert uti == oi, f"Mismatch on {k}"
        for k in self.under_test.keys():
            if not test(k, *locations):
                assert k in self.original, f"Unexpected added location {k}"
        return True
                    
    
    def check_invariants(cls, to_check):
        return True
    
    @classmethod
    def create(cls, request):
        fixture = cls(*request.param)
        fixture.protect()
        yield fixture
        fixture.check_invariants(fixture.original)
        fixture.check_invariants(fixture.under_test)
        
    
    def check_iterators(self, items):
        for x in self.under_test.items():
            assert x in items

        keys = {k for k, v in items}
        for x in self.under_test.keys():
            assert x in keys

        values = [v for k, v in items]
        for v in self.under_test.values():
            assert v in values