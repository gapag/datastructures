from gapag.ds.maps import Composition, PartialComposition

data_add = [
 # 'key        ,value',
        [(1,), 2],
        [(1, 2, 'a'), 'sborro']
    ]


data_miss = [
        #'key'
        (1, 2, 'a'),
        (1, 'fsd'),
        'fdsgdggfd'
    ]

data_overwrite = [
    #'key        ,value1         ,value2', 
    [(1, 1), 2, 3],
    [(1,), 2, 3],
    [(1,), 2, [3]],
    [('a mixed', 12, 'key'), "replace this value", "with this value"]
    ]

data_delete_branch = [
    #'key      ,value', 
        [(1,), 2],
        [(1, 2, 4), 'delete_me'],

    ]


data_trie_fixture_params=[
    [[([1, 1], 4),
     ([1, 2], 9),
     ([2], 510)]],
    [[]]
]

data_iterators = ( 
#'trie_indirect, items', 
                
                    [[x],x+y] for x,y in 
                    [ 
                     ([ # to fill the mapping
                        ((1, 1), 4),
                        ((1, 2), 9),
                        ((2,), 510)
                        ],
                        [# the item implicitly inserted which need to be considered
                         # as part of the expected result of a call to UserDict.items() 
                            ((1,), None)     
                         ]
                     )
                    ]
                )

             


data___str__ = [
    #"trie_indirect, representation"
    (
[  # entries and additional arguments such as the original and under test
    # classes
    [#entries
        
        [[1, 2, 3, 4], "value 1234"]
    ]
],
"""1 : None
    |2 : None
    |    |3 : None
    |    |    |4 : value 1234"""
                         )
]


class FAIL: pass


C = Composition
P = PartialComposition
empty = ({},)
empties = ({}, {}, {}, {},)
some = ({4: 5, 6: 2, 'df': 100, 'aa': True},
        {True: 10, False: 'gfdsgfd', 100: 20},
        {20: 'aaa', 10: 'bbb'},
        {'43': [], 'aaa': 'end'},)


data_composition = [#'cls, mappings, get, value',
                         
                             (C, empty, 'sf', FAIL),
                             (C, empties, 'sf', FAIL),
                             (C, some, 'sf', FAIL),
                             (C, some, 'aa', FAIL),
                             (C, some, 'df', 'end'),
                             (P, empty, 'sf', FAIL),
                             (P, empties, 'sf', FAIL),
                             (P, some, 'sf', FAIL),
                             (P, some, 'aa', 'bbb'),
                             (P, some, 'df', 'end'),
                             (P, some, 20, 'end'),
                             (P, some, True, 'bbb'),
]
                         
