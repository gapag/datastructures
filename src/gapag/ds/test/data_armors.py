data_ut_fixture_params = [
    [[
        ('the/root/of/the/problem', 1),
        ('the/root/of/', 32),
        ('the/root/of/the', 'ahec'),
        ('the/source/of/wisdom', 'is here'),
    ]],
    [[]]
    
]

                    
data_add = [
    #'key, value',
        ('this one', 0),
        ('this one', 1),
        ('this one/and other', 4),
    ]



data_miss = [
#    'key, value',[
        ('the/root/of/the/problem', 1),
        ('the/root/of/', 32),
        ('the/root/of/', 32),
    ]



data_overwrite = [
    #'key, value1, value2',
    
        ['/Amusicola/radio/kernshaw', 3, 2],
        ['musicola/fan', 3, 2],
        ['fromhere/to/eternity', [3], [2]],
    ]



data_del = [
#    'key, value',[
        ['orrendo/horrible', 4],
        ['harb', 67],
        ['gfdf/dsf/bfgs', 65],
    ]
