import pytest

from gapag.ds.maps import *
from gapag.ds.test.data_trie import data_composition, FAIL, data___str__, \
    data_iterators, data_trie_fixture_params, data_delete_branch, \
    data_overwrite, data_miss, data_add
from gapag.ds.test.fixtures import MappingFixture


class ADTSpec:
    pass


class IsProtected(Exception): pass


class ProtectedTrie(AbstractTrie):
    _protected = False

    def protect(self):
        self._protected = True

    def protected(fun):
        def _p_(trie, *args):
            if trie._protected:
                raise IsProtected()
            else:
                return fun(trie, *args)

        return _p_

    @protected
    def __setitem__(self, key, value):
        super(ProtectedTrie, self).__setitem__(key, value)

    @protected
    def __delitem__(self, key):
        super(ProtectedTrie, self).__delitem__(key)


class InvariantChecker(Visitor):
    def AbstractTrie(self, item, env):
        assert item.data.keys() == item.children.keys()
        for at in item.children.values():
            self.visit(at, env)


class TrieFixture(MappingFixture):

    original_cls = ProtectedTrie
    under_test_cls = AbstractTrie
    retrieve_default = lambda m,k : m[k][-1] 
    
    def protect(self):
        self.original.protect()
    
    def prefixes_(self, key, *locations):
        """
        return true if any in locations is prefix to key
        :param key: 
        :param locations: 
        :return: 
        """
        for x in locations:
            try:
                if key[:len(x)] == x:
                    return True
            except:
                pass
        return False

    def is_(self, key, *locations):
        return key in locations

    def modifies(self, *locations):
        return super(TrieFixture, self).modifies(self.is_, 
                                                 *locations)
        

    def modifies_prefix(self, *locations):
        return super(TrieFixture, self).modifies(self.prefixes_, 
                                                 *locations)
        
    
    def insertion_info(self, key):
        insertion = list(key)
        # Find the longest subkey that is already present in trie
        inserted = []
        while insertion:
            try:
                self.original[insertion]
            except KeyError:
                inserted.append(tuple(insertion))
                insertion.pop()
            else:
                break
        return insertion, inserted
    
    @classmethod
    def check_invariants(cls, to_check):
        InvariantChecker().visit(to_check, None)


@pytest.mark.parametrize(
    'key        ,value', data_add
)
def test_add(trie, key, value):
    trie.under_test[key] = value
    cumulative = []
    for x in range(1, len(key)):
        cumulative.append(trie.under_test.leaf(key[:x]))

    assert trie.under_test[key] == cumulative + [
        value] == trie.under_test.get(key, None)
    assert trie.modifies(key, *trie.insertion_info(key)[1])


@pytest.mark.parametrize(
    'key       ', data_miss
)
def test_miss(trie, key):
    with pytest.raises(KeyError):
        trie.under_test[key]
    trie.modifies()


@pytest.mark.parametrize(
    'key        ,value1         ,value2', data_overwrite)
def test_overwrite(trie, key, value1, value2):
    trie.under_test[key] = value1
    trie.under_test[key] = value2
    cumulative = []
    for x in range(1, len(key)):
        cumulative.append(trie.under_test.leaf(key[:x]))

    assert trie.under_test[key] == cumulative + [value2] == trie.under_test.get(
        key, None)
    assert trie.under_test.leaf(key) == value2
    assert trie.modifies(key, *trie.insertion_info(key)[1])


@pytest.mark.parametrize(
    'key      ,value', data_delete_branch )
def test_delete_branch(trie, key, value):
    trie.under_test[key] = value
    insertion, inserted = trie.insertion_info(key)

    # Remove the key
    del trie.under_test[key]

    # Verify that trying to get the item just removed results in a KeyError 
    with pytest.raises(KeyError):
        trie.under_test[key]

    # Check that all the keys inserted are now None 
    for x in inserted[1:]:
        assert trie.under_test.leaf(x) == None
        assert trie.under_test[x] == [None] * len(x)

    try:
        check_prefix = len(trie.original.root_at(insertion))
    except KeyError:
        check_prefix = False

    if check_prefix:
        assert trie.modifies_prefix(tuple(insertion))
    else:
        assert trie.modifies(*inserted)


@pytest.fixture(params=data_trie_fixture_params)
def trie(request):
    yield from TrieFixture.create(request)

@pytest.mark.parametrize('trie_indirect, items', 
                data_iterators,
             indirect=['trie_indirect'])
def test_iterators(trie_indirect, items):
    trie_indirect.check_iterators(items)

@pytest.fixture(scope='function')
def trie_indirect(request):
    yield from TrieFixture.create(request)


@pytest.mark.parametrize("trie_indirect, representation",
                         data___str__,
                         indirect=['trie_indirect'])
def test___str__(trie_indirect, representation):
    assert str(trie_indirect.under_test) == representation

    

@pytest.mark.parametrize('cls, mappings, get, value',
                         data_composition
                         )
def test_composition(cls, mappings, get, value):
    ut = cls(*mappings)
    if value is FAIL:
        with pytest.raises(KeyError):
            ut[get]
    else:
        assert ut[get] == value
