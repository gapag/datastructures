import pytest

from gapag.ds.armors import *
from gapag.ds.test.data_armors import data_del, data_add, data_overwrite, \
    data_miss, data_ut_fixture_params
from gapag.ds.test.fixtures import MappingFixture


class UnixPathFixture(MappingFixture):
    
    original_cls = lambda : UnixPath(Parented())
    under_test_cls = lambda : UnixPath(Parented())
    retrieve_default = lambda m, x : m[x][-1]
    
    def protect(self):
        self.original = WriteProtect(self.original)
       
@pytest.fixture(params=data_ut_fixture_params)
def ut(request):
    yield from UnixPathFixture.create(request)
                    

@pytest.mark.parametrize(
    'key, value1, value2', data_overwrite
)
def test_overwrite(ut, key, value1, value2):
    ut.under_test[key] = value1
    ut.under_test[key] = value2
    assert ut.retrieve(ut.under_test, key) == value1
    assert ut.retrieve(ut.under_test, key) == value2
    
    m = key.split(ut.under_test.separator)
    locs = []
    for y in range(1,len(m)):
        locs.append(m[:y]) 
            
    ut.modifies(locs)
    
@pytest.mark.parametrize(
    'key, value',data_add
)
def test_add(ut, key, value):
    pass

@pytest.mark.parametrize(
    'key, value',   data_del
)
def test_del(ut, key, value):
    pass

@pytest.mark.parametrize(
    'key, value',   data_miss
)
def test_miss(ut, key, value):
    pass