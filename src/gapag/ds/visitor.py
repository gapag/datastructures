from typing import Any, Dict, Callable, Type

_callback_ = Callable[[Any,Any], Any]

class DispatchFailure(Exception):
    pass

class Visitor:
    def __init__(self):
        self._literal_cache: Dict[Any, _callback_] = \
            {}
        self._type_cache: Dict[Type, _callback_] = \
            {}

    def _dispatch(self, item, env=None):
        _cb_ = None
        try:
            try:
                _cb_ = self._literal_cache[item]
            except TypeError:
                raise KeyError
        except KeyError:
            nm = type(item).__name__
            try:
                _cb_ = getattr(self, nm)
            except AttributeError:
                for x in type(item).mro():
                    try:
                        _cb_ = self._type_cache[x.__name__]
                    except KeyError:
                        try:
                            _cb_ = getattr(self, x.__name__)
                        except AttributeError:
                            pass
        if _cb_ is None:
            return self.default_visit(item, env)
        else:
            return _cb_(item, env)

    def visit(self, item, env=None):
        return self._dispatch(item, env)


    def default_visit(self, item, env=None):
        return item

    