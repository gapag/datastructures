import textwrap
from collections import UserDict
from collections.abc import MutableMapping
from itertools import chain, repeat
from typing import Any, Dict, Iterable

from gapag.ds.visitor import Visitor


class Visitable:
    def accept(self, visitor):
        raise NotImplementedError("")


class EmptyKey(Exception):
    pass

class DefaultPrettyPrinter(Visitor):
    def __init__(self):
        super(DefaultPrettyPrinter, self).__init__()
    
    def AbstractTrie(self, item, env):
        
        for key, value in item.data.items():
            env.append(f'{key} : '+self.visit(value, env))
            try:
                nested = item.children[key]
            except KeyError:
                pass
            else:
                if len(nested):
                    with env.nest():
                        chi = self.visit(nested, env)
                    
        return env.string()
            
    
    def default_visit(self, item, env=None):
        return str(item)

class Indenter:
    def __init__(self):
        
        self._current = []
        self._buffer = [self._current]
        self.space = " "
        self.indent_size = 4
        
    def append(self, item):
        self._current.append(item)
        
    def nest(self):
        return self
    
    def __enter__(self):
        self._current = []
        self._buffer.append(self._current)
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        removed = self._buffer.pop()

        m = '\n'.join(removed)
        m = textwrap.indent(m, self.space * self.indent_size  + "|")
        try:
            self._current = self._buffer[-1]
        except IndexError:
            self._current = [m]
            self._buffer.append(self._current)

        else:
            self._current.append(m)

        return m

    def string(self):
        return '\n'.join(self._current)

class Skip: pass

SKIP = Skip()



class AbstractTrie(UserDict, MutableMapping, Visitable):
    __pp = DefaultPrettyPrinter()

    def __init__(self, **kwargs):
        super(AbstractTrie, self).__init__()
        self.children: Dict[Any, AbstractTrie] = {}
        """invariant: self.children.keys() == self.data.keys()"""
        
    def __eq__(self, other):
        try:
            return self.data == other.data and \
               self.children == other.children
        except AttributeError:
            return False
        
    def __len__(self) -> int:
        return super().__len__()

    def __delitem__(self, key) -> None:
        try:
            self.delete_branch(key)
        except TypeError:
            del self.data[key]
            del self.children[key]

    def __iter__(self) :
        return super().__iter__()

    def __contains__(self, key: object) -> bool:
        try:
            self[key]
            return True
        except:
            return False
        
    def __setitem__(self, item, value):
        try:
            self.set_branch(item, 
                            chain(
                                repeat(SKIP, len(item)-1), 
                                [value]
                                
                            ))
        except TypeError:
            self.set_branch([item],
                            [value])
    
    def __getitem__(self, item):
        try:
            return self.get_branch(item)
        except TypeError:
            return self.data[item]
        
    
    def set_branch(self, key:Iterable, value:Iterable):
        
        curr = self
        hit = 0
        for c_key, c_val in zip(key, value):
            hit +=  1
            if c_key is PARENT:
                curr = curr.parent
                continue
            elif c_key is ROOT:
                curr = curr.root()
                continue
                
            
            if c_val is SKIP:
                if c_key not in curr.data:
                    curr.data[c_key] = None
            else:
                curr.data[c_key] = c_val
            try:
                curr.children[c_key]
            except:
                curr.children[c_key] = type(curr)(parent=curr)
            finally:
                curr = curr.children[c_key]
        if not hit:
            raise ValueError(f"{key}: empty key not allowed")
    
    def get_branch(self, key:Iterable):
        return self._get_branch(key)[0]
    
    def root_at(self, key:Iterable):
        return self._get_branch(key)[1]
    
    def root(self):
        raise NotImplementedError()
    
    def _get_branch(self, key:Iterable):
        curr = self
        v = []
        for x in key:
            if x is ROOT:
                curr = curr.root()
            elif x is PARENT:
                curr = curr.parent
            else:
                v.append(curr.data[x])
                curr = curr.children[x]
        return v,curr
    
    def delete_branch(self, key):
        """
        Delete only the tip
        :param key: 
        :return: 
        """
        curr = self
        v = []
        for x in key[:-1]:
            if x is ROOT:
                curr = curr.root()
            elif x is PARENT:
                curr = curr.parent
            else:
                v.append((x, curr))
                curr = curr.children[x]
        del curr.data[key[-1]]
        del curr.children[key[-1]]
                
    
    def leaf(self, key, default=None):
        return self.get(key, [default])[-1]
    
    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default
    
    def items(self):
        return Items(self)
    
    def keys(self):
        return Keys(self)
                
    def values(self):
        return Values(self)

    def __str__(self):
        return self.__pp.visit(self, Indenter())


class Parented(AbstractTrie):
    def __init__(self, parent=None, **kwargs):
        super(Parented, self).__init__(**kwargs)
        self.parent = parent
        
    def root(self):
        parent = self
        while parent.parent:
            parent = parent.parent
        return parent

class PARENT: pass
class ROOT: pass


class Walker(Visitor):
    def __init__(self, trie):
        super(Walker, self).__init__()
        self._trie = trie
    
    def __iter__(self):
        return self.visit(self._trie, None)
    

class Items(Walker):

    def AbstractTrie(self, item, env):
        yield from (((x,), y) for x, y in item.data.items())
        for x in item.children.keys():
            yield from (((x,) + k, y) for k, y in
                        self.visit(item.children[x], env))

class Keys(Walker):
    
    def AbstractTrie(self, item, env):
        yield from ((y,) for y in item.data.keys())
        for x in item.children.keys():
            yield from ((x,)+y for y in self.visit(item.children[x], env))


class Values(Walker):

    def AbstractTrie(self, item, env):
        yield from item.data.values()
        for x in item.children.keys():
            yield from self.visit(item.children[x], env)


class Composition:
    """
    Composes multiple mappings, so that 

        di = ComposedMapping(m_1, ... , m_n)
        di['a key'] == m_n[... m_1['a key']...]
    
    """

    def __init__(self, *mappings):
        self.mappings = mappings

    def __getitem__(self, item):
        key = item
        for x in self.mappings:
            key = x[key]
        return key


class PartialComposition(Composition):
    """
    Composes multiple mappings, so that 

        di = PartialComposition(m_1, ... , m_n)
        di['a key'] == m_j[... m_k['a key']...]

    for some sequence of mappings `m_j,..., m_k` , `1 <= j <= k <=n`

    example:

        PartialComposition({'a':2}, {'b':5}, {'c':4}, {5:'lalala'})['b'] == 'lalala'
        
    """

    def __getitem__(self, item):
        key = item
        misses = 0
        for x in self.mappings:
            try:
                key = x[key]
            except KeyError:
                misses += 1
        if misses == len(self.mappings):
            raise KeyError(item)
        return key

        

T = AbstractTrie



if __name__ == '__main__':
    
    tr = T.fromkeys(['abcde', 'abcdf','ds', [1,2,3], 2, 4], 'sborro')
    tr['a'] = []
    print(tr)
    
    