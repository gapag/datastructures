import re
from collections import UserDict, UserList
from collections.abc import MutableMapping

from gapag.ds.maps import AbstractTrie, PARENT, ROOT


class MappingArmor(UserDict, MutableMapping):
    """
    Wraps a mapping; calls are forwarded to self.data in case of missing 
    attribute. The rationale is to create an additional processing layer
    to a mapping. 
    """

    def __init__(self, mapping):
        super(MappingArmor, self).__init__()
        self.data = mapping
        
    def __str__(self):
        return f"<{type(self).__name__}>"+str(self.data)
    
    def __getattr__(self, item):
        try:
            m = getattr(self.data, item)
        except:
            raise
        return m


class WriteViolation(BaseException):
    pass


class WriteProtect(MappingArmor):

    def __writing(self, key=''):
        raise WriteViolation(key)

    def items(self):
        return self.data.items()

    def values(self):
        return self.data.values()

    def keys(self):
        return self.data.keys()

    def __setitem__(self, key, value):
        self.__writing(key)

    def __delitem__(self, key):
        self.__writing(key)

    def clear(self) -> None:
        self.__writing()

    def pop(self, k):
        self.__writing(k)

    def popitem(self):
        self.__writing()

    def setdefault(self, k, default):
        self.__writing(k)

    def update(self, __m, **kwargs):
        self.__writing(__m)

    def get(self, k):
        self.__writing(k)


class MultiMapping(MappingArmor):
    """Makes a multimapping of the given mapping, meaning that each value is
    a collection of type given."""

    def __init__(self, mapping, default_type=list,
                 set_fun=list.append,
                 del_fun=list.pop,
                 get_fun=None):
        super(MultiMapping, self).__init__(mapping)
        self.default_type = default_type
        self.set_fun = set_fun
        if isinstance(mapping, AbstractTrie):
            self.get_from_mapping = lambda k : self.data[k][-1]
        else:
            self.get_from_mapping = mapping.__getitem__
    
    def __setitem__(self, key, value):
        try:
            v = self.get_from_mapping(key)
        except KeyError:
            v = self.default_type()
            self.data[key] = v
        finally:
            self.set_fun(v, value)
            

class DelegationFailure(Exception):
    pass


class KeyScheme(MappingArmor):
    """Transforms the key before applying it to the wrapped mapping"""
    def __getattr__(self, item):
        try:
            m = getattr(self.data, item)
        except:
            raise
        else:
            # Periphrasis to mean type of a method...
            if isinstance(m, type(self.__init__)):
                def delegate(key, *args, **kwargs):
                    try:
                        return m(self._key_(key), *args, **kwargs)
                    except KeyError:
                        raise
                    except Exception as e:
                        raise DelegationFailure(
                            f"Delegation failed; {type(self).__name__} assumes "
                            f"first argument is key to the mapping")
                return delegate
            else:
                return m

class Aliasing(KeyScheme):
    def __init__(self, mapping, func):
        self.func = func
        self.data = mapping

    def __getitem__(self, item):
        try:
            return super(Aliasing, self).__getitem__(item)
        except:
            return super(Aliasing, self).__getitem__(self.func(item))

class Key(UserList):
    def __init__(self, initlist, absolute):
        super(Key, self).__init__(initlist)
        self.absolute = absolute

class UnixPath(KeyScheme):

    def __init__(self, *args,
                 separator='/',
                 level_up='..',
                 this_level='.',
                 name_regexp = '[A-Za-z0-9_]*$'
                 ):
        super(UnixPath, self).__init__(*args)
        self.separator = separator
        self.name_regexp = re.compile(name_regexp)
        self.actions = {
            level_up : self._level_up_,
            this_level : self._this_level_,
        }
        
        
    def _key_(self, keystring):
        key = []
        if keystring and keystring.startswith(self.separator):
            key.append(ROOT)
        chunks = keystring.split(self.separator)
         
        for x in chunks:
            try:
                self.actions[x](key)
            except:
                if self.name_regexp.match(x):
                    key.append(x)
        return tuple(key)
    
    def _keyinv_(self, keystring):
        return self.separator.join(keystring)
    
    def _level_up_(self, key):
        try:
            key.pop()
        except:
            key.append(PARENT)
    
    def _this_level_(self, key):
        pass
    

    def __getitem__(self, item):
        return super(UnixPath, self).__getitem__(self._key_(item))

    def __setitem__(self, key, value):
        return super(UnixPath, self).__setitem__(self._key_(key), value)
    
    def __delitem__(self, key):
        return super(UnixPath, self).__delitem__(self._key_(key))

    def clear(self) -> None:
        super().clear()

    def pop(self, k):
        return super().pop(self._key_(k))

    def popitem(self):
        return super().popitem()

    def setdefault(self, k, default):
        super(UnixPath, self).setdefault(self._key_(k), default)
        
    def update(self, __m , **kwargs):
        raise NotImplemented()

    def get(self, k) :
        return super().get(self._key_(k))

    def items(self) :
        return ((self._keyinv_(k),v) for (k,v) in self.data.items())

    def keys(self) :
        return(self._keyinv_(k) for k in self.data.keys())

    def values(self) :
        return self.data.values()
    
    def __contains__(self, item):
        return super(UnixPath, self).__contains__(self._key_(item))
    
    def __rshift__(self, other):
        m = UnixPath(self.root_at(other))
        m.separator = self.separator
        m.name_regexp = self.name_regexp 
        m.actions = self.actions 
        return m

