Datastructures
-----

```
.
└── src
    └── gapag
        └── ds
            ├── armors.py
            ├── maps.py
            └── visitor.py
```


In `maps.py`:
-------------

1. Trie: A tree of dictionaries;

        t = Parented()
        t['abc'] = 4
        >>> print(t['abc'])
        [None, None, 4] # return the sequence of values corresponding
                        # to the sequence of keys
        t['ab'] = 2
        >>> print(t['abc'])
        [None, 2, 4] 
        >>> print(t.leaf('ab'))
        2 # return only the last item
                                                                                
    Tries support the `keys()`, `items()`, and `values()` iteration supported by mappings in Python. The walk is depth first.
     
2. Composition: 
    `Composition` composes multiple mappings `m_1, ... , m_n`, so that 

        di = Composition(m_1, ... , m_n)
        di['a key'] == m_n[... m_1['a key']...]
    
    `PartialComposition` is instead a `Composition` where 
    
        di = PartialComposition(m_1, ... , m_n)
        di['a key'] == m_j[... m_k['a key']...]       
    
    for some sequence of mappings `m_j,..., m_k` , `1 <= j <= k <=n`

    example:
    
        PartialComposition({'a':2}, {'b':5}, {'c':4}, {5:'lalala'})['b'] == 'lalala'

    In short, if there is a miss:
     1. in `Composition` a `KeyError` is raised.
     2. in `PartialComposition` the search for the value as key in the next mapping continues further in the given mapping sequence.
     
In `armors.py`:
------------

A mapping armor is a convenience abstraction for wrapping a mapping object.
The calls are forwarded to `self.data` in case of missing 
    attribute in the wrapping ```MappingArmor```.

`WriteProtect` is a mapping armor which raises an exception when invoking a method that changes the wrapped mapping.

    t = Parented()
    t['abc'] = 4
    t = WriteProtect(t)

    >>> print(t['abc'])
    [None, None, 4]
    >>> t['abc'] = 21
    *raises WriteViolation*

A `KeyScheme` mapping armor is a key preprocessor.
Normally a trie is accessed with an iterable:

    t = Parented()
    key = ['a', 'long', 'key']
    t[key] = 4

    >>> print(t[key])
    [None, None, 4]
    
If this is verbose and you need a swifter way to access the mapping, you can
use the `UnixPath` keyscheme which transparently maps a Unix style path to a proper key for a ```Parented``` trie:

    t = UnixPath(Parented())
    key = 'a/long/key'
    t[key] = 4

    >>> print(t[key])
    [None, None, 4]

In `visitor.py`
----
Implementation of a Visitor pattern. See https://bitbucket.org/gapag/vysitors/ for a more extended implementation and use that in case you want to use the visitor pattern.